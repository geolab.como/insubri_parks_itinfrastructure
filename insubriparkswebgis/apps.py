from django.apps import AppConfig


class InsubriparkswebgisConfig(AppConfig):
    name = 'insubriparkswebgis'
