/**
 * BaseMap layers
 */
var baseLayers = new ol.layer.Group({
  title: 'Basemaps',
  openInLayerSwitcher: false,
  baseLayer: true,
  layers: [
    new ol.layer.Tile({
      title: "OSM",
      baseLayer: true,
      source: new ol.source.OSM(),
      opacity: 0.7
    }),
    new ol.layer.Tile({
      title: "Toner",
      baseLayer: true,
      visible: false,
      opacity: 0.7,
      source: new ol.source.Stamen({ layer: 'toner' })
    }),
    new ol.layer.Tile({
      title: "Watercolor",
      baseLayer: true,
      visible: false,
      opacity: 0.7,
      source: new ol.source.Stamen({ layer: 'watercolor' })
    })
  ]
})

/**
 * Spina Verde Layers
 */



/**
 * Vector layers Spina Paths
 */
var spathsSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function() {
    return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
    //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
    'version=1.0.0&request=GetFeature&' +
    'typeName=Insubriparks:paths&maxFeatures=1000&' +
    'outputFormat=application%2Fjson' +
    '&srsname=EPSG:4326';
  },
  strategy: ol.loadingstrategy.bbox
});

var lightStroke = new ol.style.Style({
  stroke: new ol.style.Stroke({
    color: 'black',
    width: 2,
  })
});

var spina_paths = new ol.layer.Vector({
	title: 'Paths '+ "<img src='/static/webgis/img/path.png' width='15' height='15' vertical-align='baseline'>",
  source: spathsSource,
  minZoom: 12.5,
  visible: true,
  style: lightStroke
  })


/**
 * Vector layers Spina Gates
 */
var sgSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function() {
    return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
    //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
    'version=1.0.0&request=GetFeature&' +
    'typeName=Insubriparks:gates&maxFeatures=1000&' +
    'outputFormat=application%2Fjson' +
    '&srsname=EPSG:4326';
  },
  strategy: ol.loadingstrategy.bbox
});

var spina_gates = new ol.layer.Vector({
	title: 'Gates '+ "<img src='/static/webgis/img/triangle.png' width='15' height='15' vertical-align='baseline'>",
  source: sgSource,
  minZoom: 11,
  visible: false,
  style: new ol.style.Style({
      image: new ol.style.RegularShape({
        fill: new ol.style.Fill({color: 'red'}),
        stroke: new ol.style.Stroke({color: 'black', width: 2}),
        points: 3,
        radius: 10,
        rotation: Math.PI / 4,
        angle: 0
      })
    })
  })

  /**
 * Vector layers Spina boundaries
 */
var spina_boundaries = new ol.layer.Image({
  title: 'Boundaries',
  visible: true,
  displayInLayerSwitcher: false,
  source: new ol.source.ImageWMS({
      url:'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/wms',
      // url:'http://localhost:8080/geoserver/Insubriparks/wms',
      params:{'LAYERS':'Insubriparks:spina_verde_boundaries'},
      crossOrigin: 'anonymous'  //Used to avoid canvas not found since this is an Image
  })
});


// /**
//  * Vector layers Spina Historic
//  */
// var shSource = new ol.source.Vector({
//   format: new ol.format.GeoJSON(),
//   url: function() {
//     return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
//     //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
//     'version=1.0.0&request=GetFeature&' +
//     'typeName=Insubriparks:historic&maxFeatures=1000&' +
//     'outputFormat=application%2Fjson' +
//     '&srsname=EPSG:4326';
//   },
//   strategy: ol.loadingstrategy.bbox
// });

// var spina_historic = new ol.layer.Vector({
// 	title: "History " + "<img src='/static/webgis/img/EDUCATION_Black-09-512.jpg' width='21' height='21' vertical-align='baseline'>",
// 	source: shSource,
//   visible: true,
//   minZoom: 12.5,
//   style: new ol.style.Style({
//     image: new ol.style.Icon({
//               src: '/static/webgis/img/EDUCATION_Black-09-512.jpg',
//               scale: 0.06
//           })
//     })
//   })


// /**
//  * Vector layers Spina Military
//  */
// var smSource = new ol.source.Vector({
//   format: new ol.format.GeoJSON(),
//   url: function() {
//     return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
//     //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
//     'version=1.0.0&request=GetFeature&' +
//     'typeName=Insubriparks:military&maxFeatures=1000&' +
//     'outputFormat=application%2Fjson' +
//     '&srsname=EPSG:4326';
//   },
//   strategy: ol.loadingstrategy.bbox
// });

// var spina_military = new ol.layer.Vector({
// 	title: 'Military ' + "<img src='/static/webgis/img/army.jpg' width='21' height='21' vertical-align='baseline'>",
//   source: smSource,
//   minZoom: 12.5,
//   visible: true,
//   style: new ol.style.Style({
//     image: new ol.style.Icon({
//               src: '/static/webgis/img/army.jpg',
//               scale: 0.06
//           })
//     })
//   })



// /**
//  * Vector layers Spina Religious
//  */
// var srSource = new ol.source.Vector({
//   format: new ol.format.GeoJSON(),
//   url: function() {
//     return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
//     //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
//     'version=1.0.0&request=GetFeature&' +
//     'typeName=Insubriparks:religious&maxFeatures=1000&' +
//     'outputFormat=application%2Fjson' +
//     '&srsname=EPSG:4326';
//   },
//   strategy: ol.loadingstrategy.bbox
// });

// var spina_religious = new ol.layer.Vector({
// 	title: 'Religion ' + "<img src='/static/webgis/img/churchicon.jpg' width='21' height='21' vertical-align='baseline'>",
//   source: srSource,
//   minZoom: 12.5,
//   visible: true,
//   style: new ol.style.Style({
//     image: new ol.style.Icon({
//               src: '/static/webgis/img/churchicon.jpg',
//               scale: 0.06
//           })
//     })
//   })



// /**
//  * Vector layers Spina Antro
//  */
// var saSource = new ol.source.Vector({
//   format: new ol.format.GeoJSON(),
//   url: function() {
//     return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
//     //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
//     'version=1.0.0&request=GetFeature&' +
//     'typeName=Insubriparks:antro&maxFeatures=1000&' +
//     'outputFormat=application%2Fjson' +
//     '&srsname=EPSG:4326';
//   },
//   strategy: ol.loadingstrategy.bbox
// });

// var spina_antro = new ol.layer.Vector({
// 	title: 'Anthropic activity ' + "<img src='/static/webgis/img/antro.jpg' width='21' height='21' vertical-align='baseline'>",
//   source: saSource,
//   minZoom: 12.5,
//   visible: true,
//   style: new ol.style.Style({
//     image: new ol.style.Icon({
//               src: '/static/webgis/img/antro.jpg',
//               scale: 0.06
//           })
//     })
//   })



// /**
//  * Vector layers Spina Sport
//  */
// var ssSource = new ol.source.Vector({
//   format: new ol.format.GeoJSON(),
//   url: function() {
//     return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
//     //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
//     'version=1.0.0&request=GetFeature&' +
//     'typeName=Insubriparks:sports&maxFeatures=1000&' +
//     'outputFormat=application%2Fjson' +
//     '&srsname=EPSG:4326';
//   },
//   strategy: ol.loadingstrategy.bbox
// });

// var spina_sport = new ol.layer.Vector({
// 	title: 'Sport ' + "<img src='/static/webgis/img/sports.jpg' width='21' height='21' vertical-align='baseline'>",
//   source: ssSource,
//   minZoom: 12.5,
//   visible: true,
//   style: new ol.style.Style({
//     image: new ol.style.Icon({
//               src: '/static/webgis/img/sports.jpg',
//               scale: 0.06
//           })
//     })
//   })



// /**
//  * Vector layers Spina Nature
//  */
// var snSource = new ol.source.Vector({
//   format: new ol.format.GeoJSON(),
//   url: function() {
//     return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
//     //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
//     'version=1.0.0&request=GetFeature&' +
//     'typeName=Insubriparks:nature&maxFeatures=1000&' +
//     'outputFormat=application%2Fjson' +
//     '&srsname=EPSG:4326';
//   },
//   strategy: ol.loadingstrategy.bbox
// });

// var spina_nature = new ol.layer.Vector({
//   title: 'Nature ' + "<img src='/static/webgis/img/rio.jpg' width='21' height='21' vertical-align='baseline'>",
//   source: snSource,
//   minZoom: 12.5,
//   visible: true,
//   style: new ol.style.Style({
//     image: new ol.style.Icon({
//               src: '/static/webgis/img/rio.jpg',
//               scale: 0.06
//           })
//     })
//   })



/**---------------------------------------------------------------------------------------------------------------------------**/
/**
 * Clustering on spina_points for test purposes
 */


/**
 * Vector layers Spina Points generic
 */
var spSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function() {
    return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
    //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
    // ciao
    'version=1.0.0&request=GetFeature&' +
    'typeName=Insubriparks:points&maxFeatures=1000&' +
    'outputFormat=application%2Fjson' +
    '&srsname=EPSG:4326';
  },
  strategy: ol.loadingstrategy.bbox
});

var spina_points = new ol.layer.Vector({
	title: 'Points of Interest',
  source: spSource,
  displayInLayerSwitcher: false,
  visible: false,
});

// Style for the clusters
var styleCache = {};
function getStyle (feature, resolution){
  var size = feature.get('features').length;
  var style = styleCache[size];
  if (!style) {
    var color = size>25 ? "192,0,0" : size>8 ? "255,128,0" : "0,128,0";
    var radius = Math.max(8, Math.min(size*0.75, 20));
    var dash = 2*Math.PI*radius/6;
    var dash = [ 0, dash, dash, dash, dash, dash, dash ];
    style = styleCache[size] = new ol.style.Style({
      image: new ol.style.Circle({
        radius: radius,
        stroke: new ol.style.Stroke({
          color: "rgba("+color+",0.5)", 
          width: 15 ,
          lineDash: dash,
          lineCap: "butt"
        }),
        fill: new ol.style.Fill({
          color:"rgba("+color+",1)"
        })
      }),
      text: new ol.style.Text({
        text: size.toString(),
        //font: 'bold 12px comic sans ms',
        //textBaseline: 'top',
        fill: new ol.style.Fill({
          color: '#fff'
        })
      })
    });
  }
  return style;
}

// a clustered source is configured with another vector source that it
// operates on

var clusterSource = new ol.source.Cluster({
  distance: 40,
  source: spSource
});

// it needs a layer too
// var styleCache = {};
// var clusterLayer = new ol.layer.Vector({
//   source: clusterSource,
//   title: 'Points of Interest '+ "<img src='/static/webgis/img/poi.png' width='30' height='20' vertical-align='baseline'>",
//   displayInLayerSwitcher: true,
//   visible: true,
//   style: function(feature) {
//     var size = feature.get('features').length;
//     var style = styleCache[size];
//     if (!style) {
//       style = new ol.style.Style({
//         image: new ol.style.Circle({
//           radius: 10,
//           stroke: new ol.style.Stroke({
//             color: '#fff'
//           }),
//           fill: new ol.style.Fill({
//             color: '#3399CC'
//           })
//         }),
//         text: new ol.style.Text({
//           text: size.toString(),
//           fill: new ol.style.Fill({
//             color: '#fff'
//           })
//         })
//       });
//       styleCache[size] = style;
//     }
//     return style;
//   }
// });

var clusterLayer = new ol.layer.AnimatedCluster({
  name: 'Points of Interest '+ "<img src='/static/webgis/img/poi.png' width='30' height='20' vertical-align='baseline'>",
  source: clusterSource,
  animationDuration: $("#animatecluster").prop('checked') ? 700:0,
  // Cluster style
  style: getStyle
});

// Style for selection
var img = new ol.style.Circle({
  radius: 6,
  stroke: new ol.style.Stroke({
    color:"rgba(0,255,255,1)", 
    width:3 
  }),
  fill: new ol.style.Fill({
    color:"rgba(0,255,255,0.3)"
  })
});
var style0 = new ol.style.Style({
  image: img
});
var style1 = new ol.style.Style({
  image: img,
  // Draw a link beetween points (or not)
  stroke: new ol.style.Stroke({
    color:"#fff", 
    width:1 
  }) 
});

// Select interaction to spread cluster out and select features
var selectCluster = new ol.interaction.SelectCluster({
  // Point radius: to calculate distance between the features
  pointRadius:7,
  animate: $("#animatesel").prop('checked'),

  // Feature style when it springs apart
  featureStyle: function(){
    return [ $("#haslink").prop('checked') ? style1:style0 ]
  },
  // selectCluster: false,	// disable cluster selection
  // Style to draw cluster when selected
  style: function(f,res){
    var cluster = f.get('features');
    if (cluster.length>1){
      var s = [ getStyle(f,res) ];
      if ($("#convexhull").prop("checked") && ol.coordinate.convexHull){
        var coords = [];
        for (i=0; i<cluster.length; i++) coords.push(cluster[i].getGeometry().getFirstCoordinate());
        var chull = ol.coordinate.convexHull(coords);
        s.push ( new ol.style.Style({
          stroke: new ol.style.Stroke({ color: "rgba(0,0,192,0.5)", width:2 }),
          fill: new ol.style.Fill({ color: "rgba(0,0,192,0.3)" }),
          geometry: new ol.geom.Polygon([chull]),
          zIndex: 1
        }));
      }
      return s;
    } else {
      return [
      new ol.style.Style({
        image: new ol.style.Circle ({
          stroke: new ol.style.Stroke({ color: "rgba(0,0,192,0.5)", width:2 }),
          fill: new ol.style.Fill({ color: "rgba(0,0,192,0.3)" }),
          radius:5
        })
      })];
    }
  }
});


// On selected => get feature in cluster and show info
selectCluster.getFeatures().on(['add'], function (e){
  var c = e.element.get('features');
  var content = "";
  console.log(c)
  if (c.length==1){
    var feature = c[0];
    var nameimg = feature.get('image')
    if (nameimg === null){
      content += "Name: ".bold().fontsize("2") + feature.get("name").fontsize("2");
      content += "<br>Category: ".bold().fontsize("2");
      if (feature.get("cat") == 'rel'){
        content += "Religion".fontsize("2")
      }
      if (feature.get("cat") == 'mil'){
        content += "Military".fontsize("2")
      }
      if (feature.get("cat") == 'hist'){
        content += "History".fontsize("2")
      }
      if (feature.get("cat") == 'nat'){
        content += "Nature".fontsize("2")
      }
      if (feature.get("cat") == 'antro'){
        content += "Anthropic activity".fontsize("2")
      }
      if (feature.get("cat") == 'sport'){
        content += "Sport".fontsize("2")
      }
      content += "<br><br><img src='/static/webgis/img/default.png'/>";
      popup.show(feature.getGeometry().getCoordinates(), content);
    } else{
        content += "Name: ".bold().fontsize("2") + feature.get("name").fontsize("2");
        content += "<br>Category: ".bold().fontsize("2");
        if (feature.get("cat") == 'rel'){
          content += "Religion".fontsize("2")
        }
        if (feature.get("cat") == 'mil'){
          content += "Military".fontsize("2")
        }
        if (feature.get("cat") == 'hist'){
          content += "History".fontsize("2")
        }
        if (feature.get("cat") == 'nat'){
          content += "Nature".fontsize("2")
        }
        if (feature.get("cat") == 'antro'){
          content += "Anthropic activity".fontsize("2")
        }
        if (feature.get("cat") == 'sport'){
          content += "Sport".fontsize("2")
        }
        content += "<br><br><img src='/static/webgis/img/"+ nameimg +"'/>";
        popup.show(feature.getGeometry().getCoordinates(), content);
    }
  } else {
    $(".infos").text("Cluster ("+c.length+" features)");
  }
})
selectCluster.getFeatures().on(['remove'], function (e){
  $(".infos").html("");
})


// REMEMBER TO DELETE map.addInteraction(selectCluster); AFTER MAP DEFINITION


/**---------------------------------------------------------------------------------------------------------------------------**/

/**
 * Popup
 */

// Popup overlay with popupClass=anim
var popup = new ol.Overlay.Popup (
  {	popupClass: "default", //"tooltips", "warning" "black" "default", "tips", "shadow",
    closeBox: true,
    onclose: function(){},
    positioning: 'auto',
    autoPan: true,
    autoPanAnimation: { duration: 250 }
  });


// /**
//  * A group for Spina Verde Park Points
//  */
// var spinaverde_points = new ol.layer.Group({
//   title: 'Points of Interest '+ "<img src='/static/webgis/img/poi.png' width='30' height='20' vertical-align='baseline'>",
//   openInLayerSwitcher: false, //because it is useful just for search function
//   visible: false,
//   layers: [spina_nature, spina_sport, spina_antro, spina_religious, spina_military, spina_historic]
// });

var spinaverde = new ol.layer.Group({
  title: 'Spina Verde Park '+ "<img src='/static/webgis/img/spina.png' width='20' height='20' vertical-align='baseline'>",
  openInLayerSwitcher: false,
  visible: true,
  layers: [spina_boundaries, spina_paths, spina_gates, clusterLayer]
});


/**
 * Map
 */
var map = new ol.Map({
target: document.getElementById('map'),  //if different docs put document.getElementById('map') otherwise just 'map'
view: new ol.View({
  zoom: 13,
  center: ol.proj.fromLonLat([9.051811, 45.812868]),
}),
controls: ol.control.defaults().extend([
    new ol.control.ScaleLine(),
    new ol.control.FullScreen({
      source: document.getElementById('map').parentNode //used to bring fullscreen also the sidebar menu
    }),
    //new ol.control.LayerPopup(),
]),
layers: [baseLayers, spinaverde],
overlays: [popup],
interactions: ol.interaction.defaults({ altShiftDragRotate:false, pinchRotate:false }),
});

map.addInteraction(selectCluster); 


/**
 * Selection system for points of interest with image
 */

// Control Select for Popup  (here to change color of selected popup)
var select = new ol.interaction.Select({
  layers: [ spina_points],
  style: new ol.style.Style({
    image: new ol.style.RegularShape({
            fill:  new ol.style.Fill({color: 'red'}),
            stroke: new ol.style.Stroke({color: 'white', width: 2}),
            points: 4,
            radius: 10,
            angle: Math.PI / 4
          })
    })
});
map.addInteraction(select);


// On selected => show/hide popup
select.getFeatures().on(['add'], function(e)
{	var feature = e.element;
  var content = "";
  var nameimg = feature.get('image');
  // content += "<img src='"+feature.get("img")+"'/>";
  // const churchImg = require("../images/church.jpeg");
  // content += "<img src='" + churchImg + "' />";
  // content += "<img src='church.jpeg' />";
  if (nameimg === null){
    content += "Name: ".bold().fontsize("2") + feature.get("name").fontsize("2");
    content += "<br>Category: ".bold().fontsize("2");
    if (feature.get("cat") == 'rel'){
      content += "Religion".fontsize("2")
    }
    if (feature.get("cat") == 'mil'){
      content += "Military".fontsize("2")
    }
    if (feature.get("cat") == 'hist'){
      content += "History".fontsize("2")
    }
    if (feature.get("cat") == 'nat'){
      content += "Nature".fontsize("2")
    }
    if (feature.get("cat") == 'antro'){
      content += "Anthropic activity".fontsize("2")
    }
    if (feature.get("cat") == 'sport'){
      content += "Sport".fontsize("2")
    }
    content += "<br><br><img src='/static/webgis/img/default.png'/>";
    popup.show(feature.getGeometry().getCoordinates(), content);
  } else{
      content += "Name: ".bold().fontsize("2") + feature.get("name").fontsize("2");
      content += "<br>Category: ".bold().fontsize("2");
      if (feature.get("cat") == 'rel'){
        content += "Religion".fontsize("2")
      }
      if (feature.get("cat") == 'mil'){
        content += "Military".fontsize("2")
      }
      if (feature.get("cat") == 'hist'){
        content += "History".fontsize("2")
      }
      if (feature.get("cat") == 'nat'){
        content += "Nature".fontsize("2")
      }
      if (feature.get("cat") == 'antro'){
        content += "Anthropic activity".fontsize("2")
      }
      if (feature.get("cat") == 'sport'){
        content += "Sport".fontsize("2")
      }
      content += "<br><br><img src='/static/webgis/img/"+ nameimg +"'/>";
      popup.show(feature.getGeometry().getCoordinates(), content);
  }
})

select.getFeatures().on(['remove'], function()
{	popup.hide();
})



/**
 * Selection system for gates with gate id
 */
var select1 = new ol.interaction.Select({
  layers: [ spina_gates ],
  style: new ol.style.Style({
    image: new ol.style.RegularShape({
            fill:  new ol.style.Fill({color: 'red'}),
            stroke: new ol.style.Stroke({color: 'white', width: 2}),
            points: 4,
            radius: 6,
            angle: Math.PI / 4
          })
    })
});
map.addInteraction(select1);

// On selected => show/hide popup
select1.getFeatures().on(['add'], function(e)
{	var feature = e.element;
  var content = "";
  content += "Gate:" + feature.get("id");
  popup.show(feature.getGeometry().getCoordinates(), content);
  }
)

select1.getFeatures().on(['remove'], function()
{	popup.hide();
})



/**
 * Selection system for paths
 */

var newstyle = new ol.style.Style({
  stroke: new ol.style.Stroke({
    color: 'red',
    width: 2,
  })
});

// Control Select for Popup  (here to change color of selected popup)
var select2 = new ol.interaction.Select({
  layers: [ spina_paths ],
  style: newstyle
});
map.addInteraction(select2);





/**
 * LayerSwitcher
 */
var ls = new ol.control.LayerSwitcher(
  { target: $("#layers > div").get(0)
}
)
map.addControl(ls);
ls.on('toggle', function(e) {
});


ls.on('drawlist', function(e) {
  if (e.layer instanceof ol.layer.Group) {
    e.li.className = e.layer.get('visible') ? 'visible': 'hidden';
  }
})

function listenVisible(layer, group) {

  layer.on('change:visible', function(e) {
    // LayerGroup
    if (layer.getLayers) {
      var vis = layer.getVisible();
      layer.getLayers().forEach( function(l) {
        if (l.getVisible() !== vis) {
          // Prevent inifnite loop
          l.set('visible', vis, true);
        }
      });
    }
    // if inside a group, check visibility of layers in it
    if (group) {
      var vis = false;
      group.getLayers().forEach( function(l) {
        if (l.getVisible()) vis = true;
      });
      if (group.getVisible() !== vis) group.setVisible(vis);
    }
  });
  // Listen to layers in it
  if (layer.getLayers) {
    layer.getLayers().forEach( function(l) {
       listenVisible(l, layer);
    });
  }
}
map.getLayers().getArray().forEach(function(l) {
  listenVisible(l);
});



/**
 * Map Zone
 */
var zone = new ol.control.MapZone({
  layer: new ol.layer.Tile({
    source: new ol.source.OSM()
  }),
  zones: [
  // {
  //   title: 'Pineta Park',
  //   extent: [8.84, 45.80, 9.04, 45.66]
  // },{
  //   title: 'Gole della Breggia Park',
  //   extent: [8.98, 45.87, 9.04, 45.84]
  // },
  {
    title: 'Spina Verde Park',
    extent: [8.94, 45.85, 9.14, 45.76],
  }]
});
map.addControl(zone);
zone.on('toggle', function(e) {
  console.log('Collapse layerswitcher', e.collapsed);
  });



/**
 * Sidebar
 */
var sidebar = new ol.control.Sidebar({ element: 'sidebar', position: 'left' });
map.addControl(sidebar);




// // This is used to allow layers visibility only if "parent" is checked
// ctrl.on('drawlist', function(e) {
// if (e.layer instanceof ol.layer.Group) {
//   e.li.className = e.layer.get('visible') ? 'visible': 'hidden';
// }
// })



/**
 * Search bar points
 */
// Set the control grid reference
var search = new ol.control.SearchFeature(
  {	//target: $(".options").get(0),
    source: spSource,
    property: 'name'
  });

map.addControl(search)



/**
 * Selected feature on search bar
 */
// Select feature when click on the reference index
search.on('select', function(e)
  {	select.getFeatures().clear();
    select.getFeatures().push (e.search);
    var p = e.search.getGeometry().getFirstCoordinate();
    map.getView().animate({ center:p });
  });



/**
 * Print Control
 */


var printControl = new ol.control.Print();
map.addControl(printControl)
document.getElementById("print_button_map").style.visibility="hidden";  //Remove print button on map

/* On print > save image file */
printControl.on('print', function(e) {
  $('body').css('opacity', .1);
});
printControl.on(['print', 'error'], function(e) {
  $('body').css('opacity', 1);
  // Print success
  if (e.image) {
    if (document.getElementById('pdf').checked) {
      // Export pdf using the print info
      var pdf = new jsPDF({
        orientation: e.print.orientation,
        unit: e.print.unit,
        format: e.print.format
      });
      pdf.addImage(e.image, 'JPEG', e.print.position[0], e.print.position[0], e.print.imageWidth, e.print.imageHeight);
      pdf.save();
    } else  {
        e.canvas.toBlob(function(blob) {
          saveAs(blob, 'map.'+e.imageType.replace('image/',''));
        }, e.imageType);
      }
  } else {
    console.warn('No canvas to export');
  }
});
