from django.urls import path

from . import views

app_name = 'insubriparkswebgis'   #used to separate different app in the html files url app: name path
urlpatterns = [
    path('', views.index, name='index'),
]

