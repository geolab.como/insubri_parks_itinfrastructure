from django.urls import path

from . import views

app_name = 'webgis'   #used to separate different app in the html files url app: name path
urlpatterns = [
    path('', views.index, name='index'),
    path('results/', views.results, name='results'),
]

