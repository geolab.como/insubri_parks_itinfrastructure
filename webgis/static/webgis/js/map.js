/**
 * BaseMap layers
 */
var baseLayers = new ol.layer.Group({
  title: 'Basemaps',
  openInLayerSwitcher: false,
  baseLayer: true,
  layers: [
    new ol.layer.Tile({
      title: "OSM",
      baseLayer: true,
      source: new ol.source.OSM(),
      opacity: 0.7
    }),
    new ol.layer.Tile({
      title: "Toner",
      baseLayer: true,
      visible: false,
      opacity: 0.7,
      source: new ol.source.Stamen({ layer: 'toner' })
    }),
    new ol.layer.Tile({
      title: "Watercolor",
      baseLayer: true,
      visible: false,
      opacity: 0.7,
      source: new ol.source.Stamen({ layer: 'watercolor' })
    })
  ]
})

/**
 * Spina Verde Layers
 */

/**
 * Vector layers Spina Points generic
 */
var spSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function() {
    return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
    //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
    // ciao
    'version=1.0.0&request=GetFeature&' +
    'typeName=Insubriparks:points&maxFeatures=1000&' +
    'outputFormat=application%2Fjson' +
    '&srsname=EPSG:4326';
  },
  strategy: ol.loadingstrategy.bbox
});

var spina_points = new ol.layer.Vector({
	title: 'Points of Interest',
  source: spSource,
  displayInLayerSwitcher: false,
  visible: true,
  style: new ol.style.Style({
  image: new ol.style.Icon({
            src: '/static/webgis/img/generic.png',
            scale: 0 //in this way it is "transparent" and at the same time it works inside the search bar
        })
  })
});


/**
 * Vector layers Spina Paths
 */
var spathsSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function() {
    return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
    //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
    'version=1.0.0&request=GetFeature&' +
    'typeName=Insubriparks:paths&maxFeatures=1000&' +
    'outputFormat=application%2Fjson' +
    '&srsname=EPSG:4326';
  },
  strategy: ol.loadingstrategy.bbox
});

var lightStroke = new ol.style.Style({
  stroke: new ol.style.Stroke({
    color: 'black',
    width: 2,
  })
});

var spina_paths = new ol.layer.Vector({
	title: 'Paths '+ "<img src='/static/webgis/img/path.png' width='15' height='15' vertical-align='baseline'>",
  source: spathsSource,
  minZoom: 12.5,
  visible: true,
  style: lightStroke
  })


/**
 * Vector layers Spina Gates
 */
var sgSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function() {
    return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
    //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
    'version=1.0.0&request=GetFeature&' +
    'typeName=Insubriparks:gates&maxFeatures=1000&' +
    'outputFormat=application%2Fjson' +
    '&srsname=EPSG:4326';
  },
  strategy: ol.loadingstrategy.bbox
});

var spina_gates = new ol.layer.Vector({
	title: 'Gates '+ "<img src='/static/webgis/img/triangle.png' width='15' height='15' vertical-align='baseline'>",
  source: sgSource,
  minZoom: 11,
  visible: false,
  style: new ol.style.Style({
      image: new ol.style.RegularShape({
        fill: new ol.style.Fill({color: 'red'}),
        stroke: new ol.style.Stroke({color: 'black', width: 2}),
        points: 3,
        radius: 10,
        rotation: Math.PI / 4,
        angle: 0
      })
    })
  })

  /**
 * Vector layers Spina boundaries
 */
var spina_boundaries = new ol.layer.Image({
  title: 'Boundaries',
  visible: true,
  displayInLayerSwitcher: false,
  source: new ol.source.ImageWMS({
      url:'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/wms',
      // url:'http://localhost:8080/geoserver/Insubriparks/wms',
      params:{'LAYERS':'Insubriparks:spina_verde_boundaries'},
      crossOrigin: 'anonymous'  //Used to avoid canvas not found since this is an Image
  })
});


/**
 * Vector layers Spina Historic
 */
var shSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function() {
    return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
    //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
    'version=1.0.0&request=GetFeature&' +
    'typeName=Insubriparks:historic&maxFeatures=1000&' +
    'outputFormat=application%2Fjson' +
    '&srsname=EPSG:4326';
  },
  strategy: ol.loadingstrategy.bbox
});

var spina_historic = new ol.layer.Vector({
	title: "History " + "<img src='/static/webgis/img/EDUCATION_Black-09-512.jpg' width='21' height='21' vertical-align='baseline'>",
	source: shSource,
  visible: true,
  minZoom: 12.5,
  style: new ol.style.Style({
    image: new ol.style.Icon({
              src: '/static/webgis/img/EDUCATION_Black-09-512.jpg',
              scale: 0.04
          })
    })
  })


/**
 * Vector layers Spina Military
 */
var smSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function() {
    return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
    //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
    'version=1.0.0&request=GetFeature&' +
    'typeName=Insubriparks:military&maxFeatures=1000&' +
    'outputFormat=application%2Fjson' +
    '&srsname=EPSG:4326';
  },
  strategy: ol.loadingstrategy.bbox
});

var spina_military = new ol.layer.Vector({
	title: 'Military ' + "<img src='/static/webgis/img/army.jpg' width='21' height='21' vertical-align='baseline'>",
  source: smSource,
  minZoom: 12.5,
  visible: true,
  style: new ol.style.Style({
    image: new ol.style.Icon({
              src: '/static/webgis/img/army.jpg',
              scale: 0.04
          })
    })
  })



/**
 * Vector layers Spina Religious
 */
var srSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function() {
    return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
    //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
    'version=1.0.0&request=GetFeature&' +
    'typeName=Insubriparks:religious&maxFeatures=1000&' +
    'outputFormat=application%2Fjson' +
    '&srsname=EPSG:4326';
  },
  strategy: ol.loadingstrategy.bbox
});

var spina_religious = new ol.layer.Vector({
	title: 'Religion ' + "<img src='/static/webgis/img/churchicon.jpg' width='21' height='21' vertical-align='baseline'>",
  source: srSource,
  minZoom: 12.5,
  visible: true,
  style: new ol.style.Style({
    image: new ol.style.Icon({
              src: '/static/webgis/img/churchicon.jpg',
              scale: 0.04
          })
    })
  })



/**
 * Vector layers Spina Antro
 */
var saSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function() {
    return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
    //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
    'version=1.0.0&request=GetFeature&' +
    'typeName=Insubriparks:antro&maxFeatures=1000&' +
    'outputFormat=application%2Fjson' +
    '&srsname=EPSG:4326';
  },
  strategy: ol.loadingstrategy.bbox
});

var spina_antro = new ol.layer.Vector({
	title: 'Anthropic activity ' + "<img src='/static/webgis/img/antro.jpg' width='21' height='21' vertical-align='baseline'>",
  source: saSource,
  minZoom: 12.5,
  visible: true,
  style: new ol.style.Style({
    image: new ol.style.Icon({
              src: '/static/webgis/img/antro.jpg',
              scale: 0.04
          })
    })
  })



/**
 * Vector layers Spina Sport
 */
var ssSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function() {
    return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
    //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
    'version=1.0.0&request=GetFeature&' +
    'typeName=Insubriparks:sports&maxFeatures=1000&' +
    'outputFormat=application%2Fjson' +
    '&srsname=EPSG:4326';
  },
  strategy: ol.loadingstrategy.bbox
});

var spina_sport = new ol.layer.Vector({
	title: 'Sport ' + "<img src='/static/webgis/img/sports.jpg' width='21' height='21' vertical-align='baseline'>",
  source: ssSource,
  minZoom: 12.5,
  visible: true,
  style: new ol.style.Style({
    image: new ol.style.Icon({
              src: '/static/webgis/img/sports.jpg',
              scale: 0.04
          })
    })
  })



/**
 * Vector layers Spina Nature
 */
var snSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function() {
    return 'http://insubriparks.como.polimi.it:8080/geoserver/Insubriparks/ows?service=WFS&' +
    //return 'http://localhost:8080/geoserver/Insubriparks/ows?service=WFS&' +
    'version=1.0.0&request=GetFeature&' +
    'typeName=Insubriparks:nature&maxFeatures=1000&' +
    'outputFormat=application%2Fjson' +
    '&srsname=EPSG:4326';
  },
  strategy: ol.loadingstrategy.bbox
});

var spina_nature = new ol.layer.Vector({
  title: 'Nature ' + "<img src='/static/webgis/img/rio.jpg' width='21' height='21' vertical-align='baseline'>",
  source: snSource,
  minZoom: 12.5,
  visible: true,
  style: new ol.style.Style({
    image: new ol.style.Icon({
              src: '/static/webgis/img/rio.jpg',
              scale: 0.04
          })
    })
  })







/**
 * A group for Spina Verde Park Points
 */
var spinaverde_points = new ol.layer.Group({
  title: 'Points of Interest '+ "<img src='/static/webgis/img/poi.png' width='30' height='20' vertical-align='baseline'>",
  openInLayerSwitcher: false, //because it is useful just for search function
  visible: true,
  layers: [spina_nature, spina_sport, spina_antro, spina_religious, spina_military, spina_historic]
});

var spinaverde = new ol.layer.Group({
  title: 'Spina Verde Park '+ "<img src='/static/webgis/img/spina.png' width='20' height='20' vertical-align='baseline'>",
  openInLayerSwitcher: false,
  visible: true,
  layers: [spina_boundaries, spina_paths, spina_gates, spinaverde_points]
});


/**---------------------------------------------------------------------------------------------------------------------------**/
/**
 * Cluster
 */



/**---------------------------------------------------------------------------------------------------------------------------**/

/**
 * Popup
 */

// Popup overlay with popupClass=anim
var popup = new ol.Overlay.Popup (
  {	popupClass: "default", //"tooltips", "warning" "black" "default", "tips", "shadow",
    closeBox: true,
    onclose: function(){},
    positioning: 'auto',
    autoPan: true,
    autoPanAnimation: { duration: 250 }
  });



/**
 * Map
 */
var map = new ol.Map({
target: document.getElementById('map'),  //if different docs put document.getElementById('map') otherwise just 'map'
view: new ol.View({
  zoom: 13,
  center: ol.proj.fromLonLat([9.051811, 45.812868]),
}),
controls: ol.control.defaults().extend([
    new ol.control.ScaleLine(),
    new ol.control.FullScreen({
      source: document.getElementById('map').parentNode //used to bring fullscreen also the sidebar menu
    }),
    //new ol.control.LayerPopup(),
]),
layers: [baseLayers, spinaverde],
overlays: [popup],
interactions: ol.interaction.defaults({ altShiftDragRotate:false, pinchRotate:false }),
});

map.addLayer(spina_points) //added here because of the search bar


/**
 * Selection system for points of interest with image
 */

// Control Select for Popup  (here to change color of selected popup)
var select = new ol.interaction.Select({
  layers: [ spina_points, spina_historic, spina_military, spina_nature, spina_antro, spina_sport, spina_religious ],
  style: new ol.style.Style({
    image: new ol.style.RegularShape({
            fill:  new ol.style.Fill({color: 'red'}),
            stroke: new ol.style.Stroke({color: 'white', width: 2}),
            points: 4,
            radius: 10,
            angle: Math.PI / 4
          })
    })
});
map.addInteraction(select);


// On selected => show/hide popup
select.getFeatures().on(['add'], function(e)
{	var feature = e.element;
  var content = "";
  var nameimg = feature.get('image');
  // content += "<img src='"+feature.get("img")+"'/>";
  // const churchImg = require("../images/church.jpeg");
  // content += "<img src='" + churchImg + "' />";
  // content += "<img src='church.jpeg' />";
  if (nameimg === null){
    content += "Name: ".bold().fontsize("2") + feature.get("name").fontsize("2");
    content += "<br>Category: ".bold().fontsize("2");
    if (feature.get("cat") == 'rel'){
      content += "Religion".fontsize("2")
    }
    if (feature.get("cat") == 'mil'){
      content += "Military".fontsize("2")
    }
    if (feature.get("cat") == 'hist'){
      content += "History".fontsize("2")
    }
    if (feature.get("cat") == 'nat'){
      content += "Nature".fontsize("2")
    }
    if (feature.get("cat") == 'antro'){
      content += "Anthropic activity".fontsize("2")
    }
    if (feature.get("cat") == 'sport'){
      content += "Sport".fontsize("2")
    }
    content += "<br><br><img src='/static/webgis/img/default.png'/>";
    popup.show(feature.getGeometry().getCoordinates(), content);
  } else{
      content += "Name: ".bold().fontsize("2") + feature.get("name").fontsize("2");
      content += "<br>Category: ".bold().fontsize("2");
      if (feature.get("cat") == 'rel'){
        content += "Religion".fontsize("2")
      }
      if (feature.get("cat") == 'mil'){
        content += "Military".fontsize("2")
      }
      if (feature.get("cat") == 'hist'){
        content += "History".fontsize("2")
      }
      if (feature.get("cat") == 'nat'){
        content += "Nature".fontsize("2")
      }
      if (feature.get("cat") == 'antro'){
        content += "Anthropic activity".fontsize("2")
      }
      if (feature.get("cat") == 'sport'){
        content += "Sport".fontsize("2")
      }
      content += "<br><br><img src='/static/webgis/img/"+ nameimg +"'/>";
      popup.show(feature.getGeometry().getCoordinates(), content);
  }
})

select.getFeatures().on(['remove'], function()
{	popup.hide();
})



/**
 * Selection system for gates with gate id
 */
var select1 = new ol.interaction.Select({
  layers: [ spina_gates ],
  style: new ol.style.Style({
    image: new ol.style.RegularShape({
            fill:  new ol.style.Fill({color: 'red'}),
            stroke: new ol.style.Stroke({color: 'white', width: 2}),
            points: 4,
            radius: 6,
            angle: Math.PI / 4
          })
    })
});
map.addInteraction(select1);

// On selected => show/hide popup
select1.getFeatures().on(['add'], function(e)
{	var feature = e.element;
  var content = "";
  content += "Gate:" + feature.get("id");
  popup.show(feature.getGeometry().getCoordinates(), content);
  }
)

select1.getFeatures().on(['remove'], function()
{	popup.hide();
})



/**
 * Selection system for paths
 */

var newstyle = new ol.style.Style({
  stroke: new ol.style.Stroke({
    color: 'red',
    width: 2,
  })
});

// Control Select for Popup  (here to change color of selected popup)
var select2 = new ol.interaction.Select({
  layers: [ spina_paths ],
  style: newstyle
});
map.addInteraction(select2);





/**
 * LayerSwitcher
 */
var ls = new ol.control.LayerSwitcher(
  { target: $("#layers > div").get(0)
}
)
map.addControl(ls);
ls.on('toggle', function(e) {
});


ls.on('drawlist', function(e) {
  if (e.layer instanceof ol.layer.Group) {
    e.li.className = e.layer.get('visible') ? 'visible': 'hidden';
  }
})

function listenVisible(layer, group) {

  layer.on('change:visible', function(e) {
    // LayerGroup
    if (layer.getLayers) {
      var vis = layer.getVisible();
      layer.getLayers().forEach( function(l) {
        if (l.getVisible() !== vis) {
          // Prevent inifnite loop
          l.set('visible', vis, true);
        }
      });
    }
    // if inside a group, check visibility of layers in it
    if (group) {
      var vis = false;
      group.getLayers().forEach( function(l) {
        if (l.getVisible()) vis = true;
      });
      if (group.getVisible() !== vis) group.setVisible(vis);
    }
  });
  // Listen to layers in it
  if (layer.getLayers) {
    layer.getLayers().forEach( function(l) {
       listenVisible(l, layer);
    });
  }
}
map.getLayers().getArray().forEach(function(l) {
  listenVisible(l);
});



/**
 * Map Zone
 */
var zone = new ol.control.MapZone({
  layer: new ol.layer.Tile({
    source: new ol.source.OSM()
  }),
  zones: [
  // {
  //   title: 'Pineta Park',
  //   extent: [8.84, 45.80, 9.04, 45.66]
  // },{
  //   title: 'Gole della Breggia Park',
  //   extent: [8.98, 45.87, 9.04, 45.84]
  // },
  {
    title: 'Spina Verde Park',
    extent: [8.94, 45.85, 9.14, 45.76],
  }]
});
map.addControl(zone);
zone.on('toggle', function(e) {
  console.log('Collapse layerswitcher', e.collapsed);
  });



/**
 * Sidebar
 */
var sidebar = new ol.control.Sidebar({ element: 'sidebar', position: 'left' });
map.addControl(sidebar);

// Open paths and gates after load results
if (open_paths == 0){
  spina_gates.setVisible(true);
  spina_paths.setVisible(true);
}


// // This is used to allow layers visibility only if "parent" is checked
// ctrl.on('drawlist', function(e) {
// if (e.layer instanceof ol.layer.Group) {
//   e.li.className = e.layer.get('visible') ? 'visible': 'hidden';
// }
// })



/**
 * Search bar points
 */
// Set the control grid reference
var search = new ol.control.SearchFeature(
  {	//target: $(".options").get(0),
    source: spSource,
    property: 'name'
  });

map.addControl(search)



/**
 * Selected feature on search bar
 */
// Select feature when click on the reference index
search.on('select', function(e)
  {	select.getFeatures().clear();
    select.getFeatures().push (e.search);
    var p = e.search.getGeometry().getFirstCoordinate();
    map.getView().animate({ center:p });
  });



/**
 * Manage Gate List Dropdown
 */

const checkbox = document.getElementById('entry')

checkbox.addEventListener('change', (event) => {
  if (event.target.checked) {
    spina_gates.setVisible(true);
  } else {
    spina_gates.setVisible(false);
  }
})



/**
 * Dropdown with features
 */
// Array of options for select dropdown -> replace with gate
var features = [];
let status = true;  // if it was var, it would be a mistake because the status variable in the if statement would not be updated.
var optionList =[];

spina_gates.getSource().on('change', function(evt){
  optionList = [];
  var source = evt.target;
  var numFeatures = source.getFeatures().length;
  features = source.getFeatures();
  if (source.getState() === 'ready' && status) {
    status = false;
    optionList = document.getElementById('entry-list').options;
    features.forEach(option =>
    optionList.add(
      new Option(option.get('id'), option.get('id'), true)
    ));

    // Order by number the gates
    $("#entry-list").html($("#entry-list option").val(function(){
      return this.text.match(/\d+/);
    }).sort(function (a, b) {
      var a = parseInt(a.value,10), b = parseInt(b.value,10);
      return a < b ? -1 : 1;
    }));

    // Next lines are used to change exit-gate list according to the selected entry-gate in the dropdown list
    $(document).ready(function () {
      $("#entry-list").change(function () {
          var val = $(this).val();

          // Next lines to select the choosen gate
          select1.getFeatures().clear(); //used to delete previous layer selected
          var step1;
          for (step1 =0; step1 < features.length; step1++){
            if (val == features[step1].get("id")){   //here first is the array with the first suggested paths
              // here there will be the path id
              select1.getFeatures().push(features[step1])   //h is passed from index.html file
              var p = features[step1].getGeometry().getFirstCoordinate();
              map.getView().animate({center: p})
            }
          }


          if (val == 2 || val == 20) {
              $("#exit-list").html("<option value='2'>2</option><option value='20'>20</option>");
          }
          if (val == 21 || val == 23 || val == 25 || val == 26 || val == 28 || val == 29 || val == 24 || val == 6 ||
              val == 35 || val == 32 || val == 38 ||  val == 39 || val == 40) {
              $("#exit-list").html("<option value='6'>6</option><option value='21'>21</option>"+
                                          "<option value='23'>23</option><option value='24'>24</option>"+
                                          "<option value='25'>25</option><option value='26'>26</option>"+
                                          "<option value='28'>28</option><option value='29'>29</option>"+
                                          "<option value='32'>32</option>"+
                                          "<option value='35'>35</option><option value='38'>38</option>"+
                                          "<option value='39'>39</option><option value='40'>40</option>");
          }
          if (val == 10 || val == 41 || val == 44 || val == 63 || val == 48 || val == 51 || val == 53) {
              $("#exit-list").html("<option value='10'>10</option><option value='41'>41</option>"+
                                          "<option value='44'>44</option><option value='51'>51</option>"+
                                          "<option value='53'>53</option><option value='53'>53</option>"+
                                          "<option value='48'>48</option><option value='63'>63</option>");
          }
          if (val == 33 || val == 55 || val == 16 || val == 60 || val == 62) {
              $("#exit-list").html("<option value='16'>16</option><option value='33'>33</option>"+
                          "<option value='55'>55</option><option value='60'>60</option>"+
                          "<option value='62'>62</option>");
          }
      });
  });

  // Following lines to select the gate choosen in the dropdown list
  $(document).ready(function () {
    $("#exit-list").change(function () {
        var val = $(this).val();
        // Next lines to select the choosen gate
        select1.getFeatures().clear(); //used to delete previous layer selected
        var step1;
        for (step1 =0; step1 < features.length; step1++){
          if (val == features[step1].get("id")){   //here first is the array with the first suggested paths
            // here there will be the path id
            select1.getFeatures().push(features[step1])   //h is passed from index.html file
            var p = features[step1].getGeometry().getFirstCoordinate();
            map.getView().animate({center: p})
          }
        }
      })
    })

  }
});



/**
 * Results of the multicriteria
 */
var paths = [];
 // if it was var, it would be a mistake because the status variable in the if statement would not be updated.
spina_paths.getSource().on('change', function(evt){
  optionList = [];
  var source_path = evt.target;
  paths = source_path.getFeatures();
});



/**
 * Click on first result and modify layers
 */
// This will be the array of the first best path
var first_div = document.getElementById("first_div")
var second_div = document.getElementById("second_div")
var third_div = document.getElementById("third_div")
var fourth_div = document.getElementById("fourth_div")
var fifth_div = document.getElementById("fifth_div")

$(first_div).click(function()
  {
    first_div.style.border = "2px solid red"
    second_div.style.border = "2px solid #22ca60"
    third_div.style.border = "2px solid #22ca60"
    fourth_div.style.border = "2px solid #22ca60"
    fifth_div.style.border = "2px solid #22ca60"

    select2.getFeatures().clear(); //used to delete previous layer selected
    var step;
    for (step =0; step < paths.length; step++){
      if (first.includes(paths[step].get("id"))){   //here first is the array with the first suggested paths
        // here there will be the path id
        select2.getFeatures().push(paths[step])   //h is passed from index.html file
        var p = paths[step].getGeometry().getFirstCoordinate();
        map.getView().animate({center: p})
      }
    }
  })



/**
 * Click on second result and modify layers
 */
$(second_div).click(function()
  {
    first_div.style.border = "2px solid #22ca60"
    second_div.style.border = "2px solid red"
    third_div.style.border = "2px solid #22ca60"
    fourth_div.style.border = "2px solid #22ca60"
    fifth_div.style.border = "2px solid #22ca60"
    select2.getFeatures().clear();
    var step;
    for (step =0; step < paths.length; step++){
      if (second.includes(paths[step].get("id"))){   //here second is the array with the second suggested paths
        // here there will be the path id
        select2.getFeatures().push(paths[step])   //h is passed from index.html file
        var p = paths[step].getGeometry().getFirstCoordinate();
        map.getView().animate({center: p})
      }
    }
  })



/**
 * Click on second result and modify layers
 */
$(third_div).click(function()
  {
    first_div.style.border = "2px solid #22ca60"
    second_div.style.border = "2px solid #22ca60"
    third_div.style.border = "2px solid red"
    fourth_div.style.border = "2px solid #22ca60"
    fifth_div.style.border = "2px solid #22ca60"

    select2.getFeatures().clear();
    var step;
    for (step =0; step < paths.length; step++){
      if (third.includes(paths[step].get("id"))){   //here second is the array with the second suggested paths
        // here there will be the path id
        select2.getFeatures().push(paths[step])   //h is passed from index.html file
        var p = paths[step].getGeometry().getFirstCoordinate();
        map.getView().animate({center: p})
      }
    }
  })



/**
 * Click on second result and modify layers
 */
$(fourth_div).click(function()
  {
    first_div.style.border = "2px solid #22ca60"
    second_div.style.border = "2px solid #22ca60"
    third_div.style.border = "2px solid #22ca60"
    fourth_div.style.border = "2px solid red"
    fifth_div.style.border = "2px solid #22ca60"

    select2.getFeatures().clear();
    var step;
    for (step =0; step < paths.length; step++){
      if (fourth.includes(paths[step].get("id"))){   //here second is the array with the second suggested paths
        // here there will be the path id
        select2.getFeatures().push(paths[step])   //h is passed from index.html file
        var p = paths[step].getGeometry().getFirstCoordinate();
        map.getView().animate({center: p})
      }
    }
  })



/**
 * Click on second result and modify layers
 */
$(fifth_div).click(function()
  {
    first_div.style.border = "2px solid #22ca60"
    second_div.style.border = "2px solid #22ca60"
    third_div.style.border = "2px solid #22ca60"
    fourth_div.style.border = "2px solid #22ca60"
    fifth_div.style.border = "2px solid red"

    select2.getFeatures().clear();
    var step;
    for (step =0; step < paths.length; step++){
      if (fifth.includes(paths[step].get("id"))){   //here second is the array with the second suggested paths
        // here there will be the path id
        select2.getFeatures().push(paths[step])   //h is passed from index.html file
        var p = paths[step].getGeometry().getFirstCoordinate();
        map.getView().animate({center: p})
      }
    }
  })



/**
 * Print Control
 */


var printControl = new ol.control.Print();
map.addControl(printControl)
document.getElementById("print_button_map").style.visibility="hidden";  //Remove print button on map

/* On print > save image file */
printControl.on('print', function(e) {
  $('body').css('opacity', .1);
});
printControl.on(['print', 'error'], function(e) {
  $('body').css('opacity', 1);
  // Print success
  if (e.image) {
    if (document.getElementById('pdf').checked) {
      // Export pdf using the print info
      var pdf = new jsPDF({
        orientation: e.print.orientation,
        unit: e.print.unit,
        format: e.print.format
      });
      pdf.addImage(e.image, 'JPEG', e.print.position[0], e.print.position[0], e.print.imageWidth, e.print.imageHeight);
      pdf.save();
    } else  {
        e.canvas.toBlob(function(blob) {
          saveAs(blob, 'map.'+e.imageType.replace('image/',''));
        }, e.imageType);
      }
  } else {
    console.warn('No canvas to export');
  }
});
