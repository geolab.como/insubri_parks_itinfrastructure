from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from .models import MCDM
import numpy as np
import pandas as pd
import psycopg2
import json

def home(request):
    return render(request, 'webgis/home.html')

def index(request):
    return render(request, 'webgis/index.html')

def handler404(request, exception):
    context = {}
    response = render(request, 'webgis/error404.html', context=context)
    response.status_code = 404
    return response


def handler500(request):
    context = {}
    response = render(request, 'webgis/error500.html', context=context)
    response.status_code = 500
    return response

def results(request):
    '''
    Extract data from user preferences
    '''
    # the following values refer to the html attribute "name"..not to the id
    h = int(request.POST['hist'])
    m = int(request.POST['mil'])
    r = int(request.POST['rel'])
    hu = int(request.POST['hum'])
    s = int(request.POST['sport'])
    n = int(request.POST['nat'])
    km = int(request.POST['km'])
    park = request.POST['park']

    if request.POST.get('entry', False):
        boolean = True
    else:
        boolean = False

    entry = request.POST.get('entry-point', False)
    exit1 = request.POST.get('exit-point', False)

    if request.POST.get('wheelchair', False):
        wheel = True
    else:
        wheel = False

    def convert(text):
        try:
            return int(text)
        except ValueError:
            pass

        try:
            return float(text)
        except ValueError:
            return text

    results = [h, m, r, hu, s, n, km, park, boolean, entry, exit1, wheel]



    '''
    Inizialization of some variables
    '''
    first_path = []
    second_path = []
    third_path = []
    fourth_path = []
    fifth_path = []

    first_cost = 0
    second_cost = 0
    third_cost = 0
    fourth_cost = 0
    fifth_cost = 0

    first_length = 0
    second_length = 0
    third_length = 0
    fourth_length = 0
    fifth_length = 0

    first_index = 0
    second_index = 0
    third_index = 0
    fourth_index = 0
    fifth_index = 0



    '''
    Pandas manipulation of the received data
    '''
    h = results[0]
    m = results[1]
    r = results[2]
    a = results[3]
    s = results[4]
    n = results[5]
    max_km = results[6]
    park = results[7]
    set_gate = results[8]
    start = results[9]
    end = results[10]
    wheelchair = results[11]

    # AHP 1st step: Criteria pairwise comparison in a big matrix, rounded at 2 decimals
    matrix = pd.DataFrame({'H': [round(h/h,2), round(h/m, 2), round(h/r,2), round(h/a,2), round(h/s, 2), round(h/n,2), ],
                        'M': [round(m/h,2), round(m/m, 2), round(m/r,2), round(m/a,2), round(m/s, 2), round(m/n,2)],
                        'R': [round(r/h,2), round(r/m, 2), round(r/r,2), round(r/a,2), round(r/s,2), round(r/n,2)],
                        'A': [round(a/h,2), round(a/m, 2), round(a/r,2), round(a/a,2), round(a/s, 2), round(a/n,2)],
                        'S': [round(s/h,2), round(s/m, 2), round(s/r,2), round(s/a,2), round(s/s,2), round(s/n,2)],
                        'N': [round(n/h,2), round(n/m, 2), round(n/r,2), round(n/a,2), round(n/s,2), round(n/n,2)]},
                        index = ['H', 'M', 'R', 'A', 'S', 'N']
                        )

    matrix = matrix.T

    # Fundamental Scale by Saaty (excel file)
    # Basically, if a is replaced by b then 1/a will be replaced by 1/b

    # Rule to be followed:
    # x_given = 5 -> x_scale = 9 : because 5 means that the pairwise comparison is 5/1. So it is 9 in the Saaty scale: extreme importance.
    matrix = matrix.replace(5, 9)
    matrix = matrix.replace(0.2, 0.11)

    # x_given = 4 -> x_scale = 8
    matrix = matrix.replace(4, 8)
    matrix = matrix.replace(0.25, 0.12)

    # x_given = 3 -> x_scale = 7
    matrix = matrix.replace(3, 7)
    matrix = matrix.replace(0.33, 0.14)

    # x_given = 2.5 -> x_scale = 6
    matrix = matrix.replace(2.5 ,6)
    matrix = matrix.replace(0.40, 0.16)

    # x_given = 2 -> x_scale = 5
    matrix = matrix.replace(2, 5)
    matrix = matrix.replace(0.50, 0.2)

    # x_given = 1.67 -> x_scale = 4
    matrix = matrix.replace(1.66, 4)
    matrix = matrix.replace(0.60, 0.25)

    # x_given = 1.50 -> x_scale = 3
    matrix = matrix.replace(1.50, 3)
    matrix = matrix.replace(0.66, 0.33)

    # x_given = 1.33 -> x_scale = 2
    matrix = matrix.replace(1.33, 2)
    matrix = matrix.replace(0.75, 0.5)

    # # x_given = 1.25 -> x_scale = 2
    matrix = matrix.replace(1.25, 2)
    matrix = matrix.replace(0.80, 0.5)

    # x_given = 1 -> x_scale = 1
    matrix = matrix.replace(1, 1)


    # For each row of 'matrix' (pairwise comparison matrix), each column is multiplied one with the others.
    # Then the final value is raised to 1/4 (1/n where n is the number of criteria)
    total = (matrix.iloc[:, :].prod(axis=1)).pow(1/4)
    sum_total = total.sum()


    # STANDARDIZATION (MAKE THE WEIGHT FROM 0 TO 1)
    weight_matrix = round(total.divide(sum_total), 2)


    # CONNECTION WITH THE DATABASE AND DERIVE MODEL
    # "score_matrix" is a matrix with all the sections and the score of h,r,s,n
    # import the data from postgresql
    try:
        connection = psycopg2.connect(user = "postgres",
                                    password = "postgres",
                                    host = "127.0.0.1",
                                    port = "5432",
                                    database = "pgrouting")

        cursor = connection.cursor()


    # query the database and store the results in a dataframe
        postgreSQL_select_Query = "select id, length, path, antro, hist, mil, nat, rel, sport from paths"
        cursor.execute(postgreSQL_select_Query)
        # print("Selecting rows from mobile table using cursor.fetchall")
        paths = cursor.fetchall()
        names = [ x[0] for x in cursor.description]
        score_matrix = pd.DataFrame( paths, columns=names)

    except (Exception, psycopg2.Error) as error :
        boolean = False
        # print ("Error while connecting to PostgreSQL", error)
    finally:
        #closing database connection.
            if(connection):
                cursor.close()
                connection.close()
                # print("PostgreSQL connection is closed")


    # score of each path (by default)
    h_score = score_matrix['hist']
    m_score = score_matrix['mil']
    r_score = score_matrix['rel']
    a_score = score_matrix['antro']
    s_score = score_matrix['sport']
    n_score = score_matrix['nat']


    # standardisation of the score (from 0 to 1) = value/max_column_value
    h_max = h_score.max()
    h_score = h_score.divide(h_max)

    m_max = m_score.max()
    m_score = m_score.divide(m_max)

    r_max = r_score.max()
    r_score = r_score.divide(r_max)

    a_max = a_score.max()
    a_score = a_score.divide(a_max)

    s_max = s_score.max()
    s_score = s_score.divide(s_max)

    n_max = n_score.max()
    n_score = n_score.divide(n_max)


    score_matrix['H'] = h_score
    score_matrix['M'] = m_score
    score_matrix['R'] = r_score
    score_matrix['A'] = a_score
    score_matrix['S'] = s_score
    score_matrix['N'] = n_score


    # update column cost of the score_matrix according to the weight_matrix
    score_matrix['cost'] = round(((score_matrix['H'] * weight_matrix['H']) +
                                (score_matrix['M'] * weight_matrix['M']) +
                                (score_matrix['R'] * weight_matrix['R']) +
                                (score_matrix['A'] * weight_matrix['A']) +
                                (score_matrix['S'] * weight_matrix['S']) +
                                (score_matrix['N'] * weight_matrix['N']) ), 2)


    score_matrix = score_matrix.drop(['antro', 'hist', 'mil', 'nat', 'rel', 'sport'], axis=1)
    paths_score = score_matrix[['id', 'path', 'cost', 'length']].copy()
    check_paths_number = False

    if set_gate:
        check_paths_number = False
        # update column cost in the postgres db because then I need it for the ksp
        try:
            connection = psycopg2.connect(user = "postgres",
                                        password = "postgres",
                                        host = "127.0.0.1",
                                        port = "5432",
                                        database = "pgrouting")

            cursor = connection.cursor()

            # ?Now it is important to create a view/temp table, update the cost column,
            # ?compute the ksp and finally drop the table
            # ?because each user must have its own updating values

            zipped_vals = zip(paths_score.id, paths_score.cost)
            tuple_to_str= str(tuple(zipped_vals))
            entries_to_update = tuple_to_str[1:len(tuple_to_str)-1] # remove first and last paren in tuple

            # Update column z by matching ID from SQL Table & Pandas DataFrame
            update_sql_query = f"""UPDATE paths p SET cost = v.cost
                                    FROM (VALUES {entries_to_update}) AS v (id, cost)
                                    WHERE p.id = v.id;"""

            cursor.execute(update_sql_query)
            connection.commit()  #COMMIT IS CRUCIAL
        except (Exception, psycopg2.Error) as error :
            boolean = False
            # print ("Error while connecting to PostgreSQL", error)
        finally:
            #closing database connection.
                if(connection):
                    cursor.close()
                    connection.close()
                    # print("PostgreSQL connection is closed")

        # pgrouting algorithm
        try:
            connection = psycopg2.connect(user = "postgres",
                                        password = "postgres",
                                        host = "127.0.0.1",
                                        port = "5432",
                                        database = "pgrouting")

            cursor = connection.cursor()

            # %s %s are used to use as input the gate insert by the users
            pgrouting_query = "SELECT path_id, edge, cost FROM pgr_ksp('SELECT * FROM paths', %s, %s, 5, directed:=false, heap_paths:=true)"
            cursor.execute(pgrouting_query, (start, end))

            routing = cursor.fetchall()
            names_columns = [ x[0] for x in cursor.description]
            routing_df = pd.DataFrame( routing, columns=names_columns)

            # paths length
            paths_length = score_matrix.loc[:, 'id':'length']
            # merge routing_df with paths_length
            routing_df = routing_df.merge(paths_length.rename(columns={'id':'edge'}),how='left')

            # sum cost of the same suggested path
            routing_costs= routing_df.groupby('path_id')['cost'].sum()
            # sum length of the same suggested path
            routing_length = routing_df.groupby('path_id')['length'].sum()
            # group by edge
            routing_edges = routing_df.groupby('path_id')['edge'].apply(list)
            # merging two previous series
            suggested_paths = pd.concat([routing_edges, routing_costs, routing_length], axis=1)
            # sort by column cost
            suggested_paths = suggested_paths.sort_values(by='cost', ascending=False)

            # Check the length according to the max value inserted by the user (it is multiplied by 1000 because the unit is meters)
            suggested_paths = suggested_paths[suggested_paths['length'] <= max_km*1000]
            # Keep only first five values
            suggested_paths = suggested_paths.head(5)

            # the following three arrays must be send to js and each one will have a different style
            first_path = np.asarray(suggested_paths['edge'].iloc[0])[:-1] #np.asarray because I need an array
            # first suggested path cost and length:
            first_cost = suggested_paths['cost'].iloc[0]
            first_length = round(float(suggested_paths['length'].iloc[0]) / 1000, 2)

            second_path = np.asarray(suggested_paths['edge'].iloc[1])[:-1]
            second_cost = suggested_paths['cost'].iloc[1]
            second_length = round(float(suggested_paths['length'].iloc[1]) / 1000, 2)

            third_path = np.asarray(suggested_paths['edge'].iloc[2])[:-1]
            third_cost = suggested_paths['cost'].iloc[2]
            third_length = round(float(suggested_paths['length'].iloc[2]) / 1000, 2)


            fourth_path = np.asarray(suggested_paths['edge'].iloc[3])[:-1]
            fourth_cost = suggested_paths['cost'].iloc[3]
            fourth_length = round(float(suggested_paths['length'].iloc[3]) / 1000, 2)


            fifth_path = np.asarray(suggested_paths['edge'].iloc[4])[:-1]
            fith_cost = suggested_paths['cost'].iloc[4]
            fifth_length = round(float(suggested_paths['length'].iloc[4]) / 1000, 2)

            # Query to reset cost = 1
            pgrouting_update_cost = "UPDATE paths SET cost=1"
            cursor.execute(pgrouting_update_cost)
            connection.commit()  #COMMIT IS CRUCIAL

        except (Exception, psycopg2.Error) as error :
            boolean = False
            # print ("Error while connecting to PostgreSQL", error)
        finally:
            #closing database connection.
                if(connection):
                    cursor.close()
                    connection.close()
                    # print("PostgreSQL connection is closed")


    else: # if no gates preferences are expressed by users
        try:
            connection = psycopg2.connect(user = "postgres",
                                        password = "postgres",
                                        host = "127.0.0.1",
                                        port = "5432",
                                        database = "pgrouting")

            cursor = connection.cursor()        
            check_paths_number = True
            # sum cost of the same suggested path
            routing_costs= paths_score.groupby('path')['cost'].sum()
            # sum length of the same suggested path
            routing_length = paths_score.groupby('path')['length'].sum()
            # group by edge
            routing_edges = paths_score.groupby('path')['id'].apply(list)
            # merging two previous series
            suggested_paths = pd.concat([routing_edges, routing_costs, routing_length], axis=1)
            # sort by column cost
            suggested_paths = suggested_paths.sort_values(by='cost', ascending=False)

            # Check the length according to the max value inserted by the user (it is multiplied by 1000 because the unit is meters)
            suggested_paths = suggested_paths[suggested_paths['length'] <= max_km*1000]
            # Keep only first five values
            suggested_paths = suggested_paths.head(5)


            # the following arrays must be send to js and each one will have a different style
            # here the last row is not removed because it is not the "-1" coming out from ksp_algorithm
            first_path = np.asarray(suggested_paths['id'].iloc[0]) #np.asarray because I need an array
            # first suggested path cost and length:
            first_cost = suggested_paths['cost'].iloc[0]
            first_length = round(float(suggested_paths['length'].iloc[0]) / 1000, 2)
            first_index = int(suggested_paths.index[0])


            second_path = np.asarray(suggested_paths['id'].iloc[1])
            second_cost = suggested_paths['cost'].iloc[1]
            second_length = round(float(suggested_paths['length'].iloc[1]) / 1000, 2)
            second_index = int(suggested_paths.index[1])

            third_path = np.asarray(suggested_paths['id'].iloc[2])
            third_cost = suggested_paths['cost'].iloc[2]
            third_length = round(float(suggested_paths['length'].iloc[2])  / 1000, 2)
            third_index = int(suggested_paths.index[2])

            fourth_path = np.asarray(suggested_paths['id'].iloc[3])
            fourth_cost = suggested_paths['cost'].iloc[3]
            fourth_length = round(float(suggested_paths['length'].iloc[3]) / 1000, 2)
            fourth_index = int(suggested_paths.index[3])


            fifth_path = np.asarray(suggested_paths['id'].iloc[4])
            fifth_cost = suggested_paths['cost'].iloc[4]
            fifth_length = round(float(suggested_paths['length'].iloc[4]) / 1000, 2)
            fifth_index = int(suggested_paths.index[4])

            # To reset cost = 1
            pgrouting_update_cost = "UPDATE paths SET cost=1"
            cursor.execute(pgrouting_update_cost)
            connection.commit()  #COMMIT IS CRUCIAL

        except (Exception, psycopg2.Error) as error :
            boolean = False
            # print ("Error while connecting to PostgreSQL", error)
        finally:
            #closing database connection.
                if(connection):
                    cursor.close()
                    connection.close()
                    # print("PostgreSQL connection is closed")



    '''
    Manipulate data before send them to results view
    '''

    if not (isinstance(first_path, list)):
        first_path = first_path.tolist()
    first_path = json.dumps(first_path)

    if (second_path != []) :
        second_path = second_path.tolist()
    second_path = json.dumps(second_path)

    if (third_path != []) :
        third_path = third_path.tolist()
    third_path = json.dumps(third_path)

    if (fourth_path != []) :
        fourth_path = fourth_path.tolist()
    fourth_path = json.dumps(fourth_path)

    if (fifth_path != []):
        fifth_path = fifth_path.tolist()
    fifth_path = json.dumps(fifth_path)


    '''
    Send all the needed variables to the result page
    '''
    return render(request, 'webgis/index.html', {'h':h, 'first':first_path, 'second':second_path,
                                                'third':third_path, 'fourth':fourth_path, 'fifth':fifth_path,
                                                'first_cost':first_cost, 'first_length':first_length,
                                                'second_cost':second_cost, 'second_length':second_length,
                                                'third_cost':third_cost, 'third_length':third_length,
                                                'fourth_cost':fourth_cost, 'fourth_length':fourth_length,
                                                'fifth_cost':fifth_cost, 'fifth_length':fifth_length,
                                                'first_index':first_index, 'second_index':second_index,
                                                'third_index':third_index, 'fourth_index':fourth_index,
                                                'fifth_index':fifth_index, 'check_paths_number':check_paths_number})
