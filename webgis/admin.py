from django.contrib import admin
from .models import MCDM
# Register your models here.

class MCDMAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['mcdm_text']}),
    ]
    

admin.site.register(MCDM, MCDMAdmin)