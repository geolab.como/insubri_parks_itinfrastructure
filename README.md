# IT infrastructure development documentation and code

The web GIS is developed using OpenLayers. The dashboard is developed using Leaflet and D3.js.

## Installation instructions

The development environment is Ubuntu 18.04.

#### 1. Follow the Django installation guide at https://docs.djangoproject.com/en/3.0/intro/install/.

##### 1.1 Install Python 3. https://linuxize.com/post/how-to-install-python-3-8-on-ubuntu-18-04/
    $ sudo apt install software-properties-common
    $ sudo add-apt-repository ppa:deadsnakes/ppa
    $ sudo apt install python3.8

##### 1.2 Set up PostgreSQL. https://docs.djangoproject.com/en/3.0/topics/install/#database-installation

###### 1.2.1 Install PostgreSQL.
    $ sudo apt install postgresql postgresql-contrib

###### 1.2.2 Install psycopg2 package.
    $ sudo apt install python-psycopg2

###### 1.2.3 Install pgAdmin4.
    $ sudo apt install apache2
    $ sudo apt-get install pgadmin4 pgadmin4-apache2

###### 1.2.4 Install PostGIS and pgRouting.
    $ sudo apt install postgresql-10-postgis-scripts
    $ sudo apt install postgresql-10-pgrouting

##### 1.3 Install pip.
    $ sudo apt install python3-pip

##### 1.4 Set up the virtual environment and install Django. https://docs.djangoproject.com/en/3.0/intro/contributing/
    $ cd ~/
    $ sudo apt-get install python3-venv
    $ python3 -m venv ~/.virtualenvs/djangodev
    $ source ~/.virtualenvs/djangodev/bin/activate
    $ sudo pip install virtualenvwrapper
    $ export WORKON_HOME=~/.virtualenvs
    $ workon djangodev
    $ python -m pip install Django
    $ deactivate

#### 2. Copy the INSUBRI.PARKS project in the folder you desire. I put it inside a folder /home/user/Django.
    $ cd /home/user/Django
    $ sudo mv insubri_parks ./
    $ workon djangodev
    $ cd insubri_parks
    $ pip install psycopg2-binary (This is necessary if in the Django settings PostgreSQL is set to be the database.)
    $ pip install tweepy
    $ pip install pandas
    $ python manage.py runserver

#### 3. Install npm. https://github.com/nodesource/distributions/blob/master/README.md
    $ curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
    $ sudo apt-get install -y nodejs

#### 4. Install the Node.js modules.
    $ cd /home/user/Django/insubri_parks/dashboard/static/dashboard
    $ npm install

#### 5. Install GeoServer. https://docs.geoserver.org/latest/en/user/installation/linux.html

#### 6. Create a new superuser.
    $ python manage.py createsuperuser

#### NOTES:
* The development started after following the complete tutorial starting at https://docs.djangoproject.com/en/3.0/intro/tutorial01/.
* Login and logout were developed following the instructions at https://wsvincent.com/django-user-authentication-tutorial-login-and-logout/.

#### STRUCTURE OF THE REPOSITORY:
* ***webgis*** folder: it contains the code of the innovative WebGIS with the MCDM tool.
* ***insubriparkswebgis*** folder: it contains the code of the INSUBRI.PARKS WebGIS which is actually under development.
* ***dashboard*** folder: it contains the code of the Analytic Dashboard.
* ***insubri_parks***, ***static***, ***templates*** folders: they contain the Django-project skeleton.

