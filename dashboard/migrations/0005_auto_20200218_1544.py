# Generated by Django 3.0.2 on 2020-02-18 15:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0004_auto_20200218_1544'),
    ]

    operations = [
        migrations.RenameField('tweet', 'point_coordinates_lan', 'point_coordinates_lat'),
        migrations.RenameField('tweet', 'rectangle_coordinates_lan_min', 'rectangle_coordinates_lat_min'),
        migrations.RenameField('tweet', 'rectangle_coordinates_lan_max', 'rectangle_coordinates_lat_max'),
    ]
