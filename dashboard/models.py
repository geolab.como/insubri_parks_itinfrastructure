from django.contrib.postgres.fields import ArrayField
from django.db import models

class Tweet(models.Model):
    created_at = models.CharField(max_length=200)
    timestamp = models.CharField(max_length=200)
    id = models.CharField(max_length=200, primary_key=True)
    text = models.CharField(max_length=400)
    source =models.CharField(max_length=400)
    user_id = models.CharField(max_length=200)
    user_screen_name = models.CharField(max_length=200)
    point_coordinates_long = models.FloatField(null=True)
    point_coordinates_lat = models.FloatField(null=True)
    place = models.CharField(max_length=200)
    rectangle_coordinates_long_min = models.FloatField()
    rectangle_coordinates_long_max = models.FloatField()
    rectangle_coordinates_lat_min = models.FloatField()
    rectangle_coordinates_lat_max = models.FloatField()
    hashtags = ArrayField(models.CharField(max_length=400), null=True)
