from django.shortcuts import render
from django.contrib.auth.decorators import login_required
# from django.db.models import Q
import pandas as pd
from .models import Tweet

def group_by_day(timestamp):
    tweets_json_aggregated_day = []

    if timestamp:
        tweets_dict_timestamp = {'timestamp': timestamp}
        tweets_df_timestamp = pd.DataFrame(tweets_dict_timestamp)

        tweets_df_timestamp.index = tweets_df_timestamp['timestamp']
        # print(tweets_df_timestamp)
        tweets_df_aggregated_day = tweets_df_timestamp.groupby(tweets_df_timestamp.index.date).count().reset_index()
        tweets_df_aggregated_day.columns = ['date', 'count']
        tweets_df_aggregated_day['date'] = tweets_df_aggregated_day['date'].astype(str)
        # print(tweets_df_aggregated_day)

        tweets_json_aggregated_day = tweets_df_aggregated_day.to_json(orient='records')
        # print(tweets_json_aggregated_day)

    return tweets_json_aggregated_day


@login_required
def index(request):
    tweets = Tweet.objects.all()

    timestamp = []
    # longitude = []
    # latitude = []
    # place_center_longitude = []
    # place_center_latitude = []

    for tweet in tweets:
        timestamp.append(pd.Timestamp(int(tweet.timestamp), unit='ms'))
        # longitude.append(tweet.point_coordinates_long)
        # latitude.append(tweet.point_coordinates_lat)
        # place_center_longitude.append((tweet.rectangle_coordinates_long_min+tweet.rectangle_coordinates_long_max)/2)
        # place_center_latitude.append((tweet.rectangle_coordinates_lat_min+tweet.rectangle_coordinates_lat_max)/2)

    '''
    tweets_dict = {'timestamp': timestamp,
                   'longitude': longitude,
                   'latitude': latitude,
                   'place_center_longitude': place_center_longitude,
                   'place_center_latitude': place_center_latitude}
    tweets_df = pd.DataFrame(tweets_dict)
    '''

    tweets_json_aggregated_day = group_by_day(timestamp)

    return render(request, 'dashboard/index.html', {'tweets_json_aggregated_day': tweets_json_aggregated_day})


def replot(request):
    hashtag = request.POST.get('hashtag', '')
    # print(hashtag)

    # tweets = Tweet.objects.filter(Q(text__icontains=hashtag)|Q(hashtags__icontains=[hashtag]))
    tweets = Tweet.objects.filter(text__icontains=hashtag)

    timestamp = []

    for tweet in tweets:
        timestamp.append(pd.Timestamp(int(tweet.timestamp), unit='ms'))
        # print(tweet.text)
        # print(tweet.hashtags)

    tweets_json_aggregated_day = group_by_day(timestamp)

    return render(request, 'dashboard/index.html', {'tweets_json_aggregated_day': tweets_json_aggregated_day})
