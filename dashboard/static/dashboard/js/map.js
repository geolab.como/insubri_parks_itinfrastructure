var cartoDark = L.tileLayer('https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png', {
	attribution: "&copy; <a href='https://www.openstreetmap.org/copyright'>OpenStreetMap</a> contributors &copy; <a href='https://carto.com/attributions'>CARTO</a>",
	subdomains: 'abcd'
});

var mapboxSatelliteStreets = L.tileLayer('http://a.tiles.mapbox.com/v4/mapbox.streets-satellite/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZ2lzZ2VvbGFiIiwiYSI6ImNrNXhzdG1sOTIycWMzbG1sc3Y4Z2RtOTkifQ.2e84S_0qspcM8oUIGwVwkQ', {
	attribution: "&copy; <a href='https://apps.mapbox.com/feedback/'>Mapbox</a> &copy; <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>"
});

var styleParks = {
	'weight': 2,
  'color': '#22ca60',
	'opacity': 1.0,
	'fillOpacity': 0.6
};

/* L.geoPackageFeatureLayer([], {
		geoPackageUrl: campoDeiFioriPath,
		layerName: 'campo_dei_fiori',
		style: styleParks
}).addTo(map); */

var boscoDelPenz = new L.geoJson([], {style: styleParks});
$.ajax({
	dataType: 'json',
	url: boscoDelPenzDirectory,
	success: function(data) {
	  $(data.features).each(function(key, data) {
	      boscoDelPenz.addData(data);
	  });
	}
});

var campoDeiFiori = new L.geoJson([], {style: styleParks});
$.ajax({
	dataType: 'json',
	url: campoDeiFioriDirectory,
	success: function(data) {
	  $(data.features).each(function(key, data) {
	      campoDeiFiori.addData(data);
	  });
	}
});

var goleDellaBreggia = new L.geoJson([], {style: styleParks});
$.ajax({
	dataType: 'json',
	url: goleDellaBreggiaDirectory,
	success: function(data) {
	  $(data.features).each(function(key, data) {
	      goleDellaBreggia.addData(data);
	  });
	}
});

var pineta = new L.geoJson([], {style: styleParks});
$.ajax({
	dataType: 'json',
	url: pinetaDirectory,
	success: function(data) {
	  $(data.features).each(function(key, data) {
	      pineta.addData(data);
	  });
	}
});

var spinaVerde = new L.geoJson([], {style: styleParks});
$.ajax({
	dataType: 'json',
	url: spinaVerdeDirectory,
	success: function(data) {
	  $(data.features).each(function(key, data) {
	      spinaVerde.addData(data);
	  });
	}
});

var map = L.map('map', {
    center: [45.805, 8.902],
    zoom: 10,
    layers: [cartoDark, boscoDelPenz, campoDeiFiori, goleDellaBreggia, pineta, spinaVerde]
});

var baseMaps = {
    'CARTO Dark': cartoDark,
		'Mapbox Satellite Streets': mapboxSatelliteStreets
};

var overlayMaps = {
    'Bosco del Penz': boscoDelPenz,
		'Campo dei Fiori': campoDeiFiori,
		'Gole della Breggia': goleDellaBreggia,
		'Pineta': pineta,
		'Spina Verde': spinaVerde
};

L.control.layers(baseMaps, overlayMaps).addTo(map);
