var width = 700;
var height = 344;

var svg = d3
  .select("#plot")
  .append("svg")
  .attr("viewBox", `0 0 ${width} ${height}`);

var margin = {left: 70, right: 12, top: 24, bottom: 66};
var width = 700 - margin.left - margin.right;
var height = 344 - margin.top - margin.bottom;

var g = svg.append("g")
  .attr("transform", `translate(${margin.left}, ${margin.top})`);

var t = function(){ return d3.transition().duration(1000); }

var parseTime = d3.timeParse("%Y-%m-%d");
var formatTime = d3.timeFormat("%d/%m/%Y");
var bisectDate = d3.bisector(function(d) { return d.date; }).left;

var time1 = parseTime("2020-02-13");
var time2 = new Date();
$("#date-label-2").text(formatTime(time2));

// console.log(time1);
// console.log(time2);

// scales
var x = d3.scaleTime().range([0, width]);
var y = d3.scaleLinear().range([height, 0]);

// axis generators
var xAxisCall = d3.axisBottom();
var yAxisCall = d3.axisLeft();

// axis groups
var xAxis = g.append("g")
  .attr("transform", "translate(0," + height + ")");
var yAxis = g.append("g");

// axis labels
var xLabel = g.append("text")
  .attr("x", width / 2)
  .attr("y", height + 50)
  .attr("font-size", "20px")
  .attr("text-anchor", "middle")
  .text("time");
var yLabel = g.append("text")
  .attr("x", -(height / 2))
  .attr("y", -50)
  .attr("font-size", "20px")
  .attr("text-anchor", "middle")
  .attr("transform", "rotate(-90)")
  .text("number of tweets");

// line path generator
var line = d3.line()
  .x(function(d) { return x(d.date); })
  .y(function(d) { return y(d.count); });

// Add line to chart.
g.append("path")
  .attr("class", "line");

$("#date-slider").slider({
  range: true,
  min: time1.getTime(),
  max: time2.getTime(),
  step: 86400000,
  values: [time1.getTime(), time2.getTime()],
  slide: function(event, ui){
    time1 = new Date(ui.values[0]);
    time2 = new Date(ui.values[1]);
    $("#date-label-1").text(formatTime(time1));
    $("#date-label-2").text(formatTime(time2));
    update();
  }
});

// console.log(tweetsJsonAggregatedDay);

tweetsJsonAggregatedDay.forEach(function(d){
  d['date'] = parseTime(d['date']);
})

update();

function update() {
  tweetsJsonAggregatedDayFiltered = tweetsJsonAggregatedDay.filter(function(d){
    return ((d.date >= time1) && (d.date <= time2))
  });

  // Set scale domains.
  x.domain([time1, time2]);
  y.domain([0, d3.max(tweetsJsonAggregatedDayFiltered, function(d) { return d.count; }) * 1.005]);

  // Generate axes once scales have been set.
  xAxis
    .transition(t)
    .call(xAxisCall.scale(x))
  yAxis
    .transition(t)
    .call(yAxisCall.scale(y).ticks(5))

  // Update the line.
  g.select(".line")
    .transition(t)
    .attr("d", line(tweetsJsonAggregatedDayFiltered));

  // Clear old tooltips.
  d3.select(".focus").remove();
  d3.select(".overlay").remove();

  /******************************** tooltip code ********************************/
  var focus = g.append("g")
    .attr("class", "focus")
    .style("display", "none");

  focus.append("line")
    .attr("class", "x-hover-line hover-line")
    .attr("y1", 0)
    .attr("y2", height);

  focus.append("line")
    .attr("class", "y-hover-line hover-line")
    .attr("x1", 0)
    .attr("x2", width);

  focus.append("circle")
    .attr("r", 7.5);

  focus.append("text")
    .attr("x", 15)
    .attr("dy", "5");

  g.append("rect")
    .attr("class", "overlay")
    .attr("width", width)
    .attr("height", height)
    .on("mouseover", function() { focus.style("display", null); })
    .on("mouseout", function() { focus.style("display", "none"); })
    .on("mousemove", mousemove);

  function mousemove() {
    var x0 = x.invert(d3.mouse(this)[0]),
    i = bisectDate(tweetsJsonAggregatedDayFiltered, x0, 1),
    d0 = tweetsJsonAggregatedDayFiltered[i - 1],
    d1 = tweetsJsonAggregatedDayFiltered[i],
    d = x0 - d0.date > d1.date - x0 ? d1 : d0;
    focus.attr("transform", "translate(" + x(d.date) + "," + y(d.count) + ")");
    focus.select("text").text(d.count);
    focus.select(".x-hover-line").attr("y2", height - y(d.count));
    focus.select(".y-hover-line").attr("x2", -x(d.date));
  }
  /******************************** tooltip code ********************************/
}
